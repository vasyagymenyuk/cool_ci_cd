const child_process = require("child_process");

//Получение списка файлов и директорий для Linux
const ls = child_process.exec("ls");

ls.stdout.on("data", (data) => console.log("Files list: \n", data));
ls.stderr.on("error", (error) => console.log("Error: \n", error));
